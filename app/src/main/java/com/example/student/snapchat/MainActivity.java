package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "E81961D3-A2E0-3467-FFD0-2F27D4D22000";
    public static final String SECRET_KEY="4C68F392-07F2-6414-FF21-73849C939700";
    public static final String VERSION = "v1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainMenuFragment mainMenu = new MainMenuFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
    }
}
